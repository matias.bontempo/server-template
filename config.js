require('dotenv');

const DB_URI = process.env.DB_URI || 'mongodb://localhost:27017/default';
const PORT = process.env.PORT || 5000;

module.exports = {
  DB_URI,
  PORT,
};
