const fastify = require('fastify');
const logger = require('./plugins/logger');

const { PORT } = require('./config');

const app = fastify({ logger });

app.get('/', async (req, res) => res.send({ success: true }));

const start = async () => {
  try {
    await app.listen(PORT);
  } catch (err) {
    app.log.error(err);
    process.exit(1);
  }
};

start();
